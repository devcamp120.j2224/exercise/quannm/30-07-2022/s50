package com.devcamp.j14.pizza365api.controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.j14.pizza365api.model.CCustomer;
import com.devcamp.j14.pizza365api.model.CDrink;
import com.devcamp.j14.pizza365api.model.CMenu;
import com.devcamp.j14.pizza365api.model.COrder;
import com.devcamp.j14.pizza365api.model.CProduct;
import com.devcamp.j14.pizza365api.model.CVoucher;
import com.devcamp.j14.pizza365api.repository.ICustomerRepository;
import com.devcamp.j14.pizza365api.repository.IDrinkRepository;
import com.devcamp.j14.pizza365api.repository.IMenuRepository;
import com.devcamp.j14.pizza365api.repository.IOrderReposiroty;
import com.devcamp.j14.pizza365api.repository.IVoucherRepository;

@CrossOrigin
@RestController
@RequestMapping("/")
public class Pizza365Controller {
    
    @Autowired
    IVoucherRepository pIVoucherRepository;
    @Autowired
    IMenuRepository pIMenuRepository;
    @Autowired
    IDrinkRepository pDrinkRepository;
    @Autowired
    ICustomerRepository pCustomerRepository;
    @Autowired
    IOrderReposiroty pIOrderReposiroty;

    @GetMapping("/vouchers")
    public ResponseEntity<List<CVoucher>> getAllVoucher() {
        try {
            List<CVoucher> listVouchers = new ArrayList<>();

            pIVoucherRepository.findAll().forEach(listVouchers::add);

            return new ResponseEntity<List<CVoucher>>(listVouchers, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/menu")
    public ResponseEntity<List<CMenu>> getAllMenu() {
        try {
            List<CMenu> listMenu = new ArrayList<>();

            pIMenuRepository.findAll().forEach(listMenu::add);

            return new ResponseEntity<List<CMenu>>(listMenu, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/drinks")
    public ResponseEntity<List<CDrink>> getAllDrink() {
        try {
            List<CDrink> listDrinks = new ArrayList<>();

            pDrinkRepository.findAll().forEach(listDrinks::add);

            return new ResponseEntity<List<CDrink>>(listDrinks, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/customers")
    public ResponseEntity<List<CCustomer>> getAllCustomer() {
        try {
            List<CCustomer> listCustomers = new ArrayList<>();

            pCustomerRepository.findAll().forEach(listCustomers::add);

            return new ResponseEntity<List<CCustomer>>(listCustomers, HttpStatus.OK);
        } catch (Exception e) {
            //TODO: handle exception
            return new ResponseEntity<>(HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/order")
	public ResponseEntity<Set<COrder>> getOrderByCustomerId(@RequestParam long customerId) {
        try {
            CCustomer vCustomer = pCustomerRepository.findById(customerId);
            
            if(vCustomer != null) {
            	return new ResponseEntity<>(vCustomer.getOrders(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/product")
	public ResponseEntity<Set<CProduct>> getProductByOrderId(@RequestParam long orderId) {
        try {
            COrder vOrder = pIOrderReposiroty.findById(orderId);
            
            if(vOrder != null) {
            	return new ResponseEntity<>(vOrder.getProducts(), HttpStatus.OK);
            } else {
            	return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }
}
