package com.devcamp.j14.pizza365api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j14.pizza365api.model.COrder;

public interface IOrderReposiroty extends JpaRepository<COrder, Long> {
    COrder findById(long order_id);
}
