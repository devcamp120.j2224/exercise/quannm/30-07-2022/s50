package com.devcamp.j14.pizza365api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j14.pizza365api.model.CCustomer;

public interface ICustomerRepository extends JpaRepository<CCustomer, Long> {
    CCustomer findById(long customer_id);
}
