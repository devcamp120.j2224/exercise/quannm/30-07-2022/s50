package com.devcamp.j14.pizza365api;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Pizza365apiApplication {

	public static void main(String[] args) {
		SpringApplication.run(Pizza365apiApplication.class, args);
	}

}
