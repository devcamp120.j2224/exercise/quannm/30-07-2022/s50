package com.devcamp.j14.pizza365api.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "menu")
public class CMenu {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private int id;

    private String name;

    @Column(name = "duong_kinh")
    private int duongKinh;

    private int suon;

    private int salad;

    @Column(name = "nuoc_ngot")
    private int nuocNgot;

    @Column(name = "thanh_tien")
    private int thanhTien;

    public CMenu() {
    }

    public CMenu(int id, String name, int duongKinh, int suon, int salad, int nuocNgot, int thanhTien) {
        this.id = id;
        this.name = name;
        this.duongKinh = duongKinh;
        this.suon = suon;
        this.salad = salad;
        this.nuocNgot = nuocNgot;
        this.thanhTien = thanhTien;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getDuongKinh() {
        return duongKinh;
    }

    public void setDuongKinh(int duongKinh) {
        this.duongKinh = duongKinh;
    }

    public int getSuon() {
        return suon;
    }

    public void setSuon(int suon) {
        this.suon = suon;
    }

    public int getSalad() {
        return salad;
    }

    public void setSalad(int salad) {
        this.salad = salad;
    }

    public int getNuocNgot() {
        return nuocNgot;
    }

    public void setNuocNgot(int nuocNgot) {
        this.nuocNgot = nuocNgot;
    }

    public int getThanhTien() {
        return thanhTien;
    }

    public void setThanhTien(int thanhTien) {
        this.thanhTien = thanhTien;
    }
    
    
}

