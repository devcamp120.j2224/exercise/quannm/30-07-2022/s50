package com.devcamp.j14.pizza365api.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.j14.pizza365api.model.CDrink;

public interface IDrinkRepository extends JpaRepository<CDrink, Long> {
    
}
