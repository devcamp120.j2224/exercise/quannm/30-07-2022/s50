package com.devcamp.j14.pizza365api.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "products")
public class CProduct {

    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private long product_id;

	private String name;

	private String type;

	private String color;

	private String price;

    @ManyToOne
    @JoinColumn(name = "order_id")
	private COrder order;

    public CProduct() {
    }

    public CProduct(String name, String type, String color, String price) {
        this.name = name;
        this.type = type;
        this.color = color;
        this.price = price;
    }

    public long getId() {
        return product_id;
    }

    public void setId(long product_id) {
        this.product_id = product_id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    
}
